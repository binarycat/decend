-- ini-based format for marshaling cyclic data

local cyclini = {}

cyclini.quote_type_tbl = {
	boolean = true,
	string = true,
	number = true,
}

function cyclini.quote(val)
	if type(val) == "string" then
		return string.format("%q", val)
	else
		return tostring(val)
	end
end

function cyclini.marshal(val, ctx)
	ctx = ctx or { ref = {}, n = 0 }
	local id = ctx.n
	ctx.n = ctx.n + 1
	local ty = type(val)
	if cyclini.quote_type_tbl[ty] then
		ctx.ref[val] = id
		return "["..id.."]\nv="..cyclini.quote(val).."\n", id
	elseif ty == "table" then
		ctx.ref[val] = id
		local ps = ""
		local vs = "["..id.."]\n"
		for k,v in pairs(val) do
			local kref = ctx.ref[k]
			if kref == nil then
				local pre
				pre,kref = cyclini.marshal(k, ctx)
				ps = ps..pre
			end
			local vref = ctx.ref[v]
			if vref == nil then
				local pre
				pre,vref = cyclini.marshal(v, ctx)
				ps = ps..pre
			end
			vs = vs..kref.."="..vref.."\n"
		end
		return ps..vs, id
	else
		error("cannot marshal value of type "..ty)
	end
end

function cyclini.eval(str)
	return load("return "..str, nil, "t", {})()
end

function cyclini._unmarshal(str, reftbl, fname)
	reftbl = reftbl or {}
	local id
	local lineno = 1
	for ln in string.gmatch(str, "([^\n]+)\n") do
		local newid = string.match(ln, "%[(%w+)%]")
		if newid then
			id = tonumber(newid)
			if reftbl[id] == nil then
				reftbl[id] = {}
			end
		else
			assert(id ~= nil, fname..":"..lineno..": cyclini syntax error")
			local k, v = string.match(ln, "(%w+)=(.+)")
			if k == "v" then
				--assert(reftbl[id] == nil,
				--	fname..":"..lineno..": 'v' in already existing value")
				reftbl[id] = cyclini.eval(v)
			else
				-- if this is the first key, we may need to create the table
				if reftbl[id] == nil then
					reftbl[id] = {}
				end
				k = tonumber(k)
				v = tonumber(v)
				--assert(reftbl[k] ~= nil and reftbl[v] ~= nil,
				--	fname..":"..lineno..": cyclini: unknown id")
				-- forward references, should be only possible with tables
				if reftbl[k] == nil then reftbl[k] = {} end
				if reftbl[v] == nil then reftbl[v] = {} end
				reftbl[id][reftbl[k]] = reftbl[v]
			end
		end
		lineno = lineno + 1
	end
	return reftbl
end

function cyclini.unmarshal(str, name)
	local ok, res = pcall(cyclini._unmarshal, str, {}, name or "[string]")
	if ok then
		if res[0] == nil then
			return nil, "cyclini: unmarshal failed, item 0 is missing"
		end
		return res[0], nil
	else
		return nil, res
	end
end

return cyclini
