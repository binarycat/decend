local decend = require("src.init")

function love.load()
	decend:init()
	-- TODO: main menu
	decend:embark()
	-- debugging stuff
	--local slime = decend:spawn(1, "slime")
	--local other = decend:spawn(1)
	--other.st.aff.v = 100
	--slime.st.aff.v = -100
	--decend:teachspell(decend.game.player, "restrain", 1)
	--decend:teachspell(decend.game.player, "absorb", 1)
	-- end debugging stuff
	decend:menuplan(decend.game.player)
	-- TODO: if there are any new keys that were not in the original config, they are likely typos, print a warning about them.
	local ok, err = pcall(function()
		local f, err = loadfile(love.filesystem.getSaveDirectory().."/config.lua", "t", decend.conf)
		if f == nil then error(err) end
		f()
	end)
	if not ok then print("WARNING: failed to load user config: "..err) end
end

-- TODO: show the "summoning sickness" of newly spawned enemies
function love.draw()
	local cam = decend.view.pos
	local gridsize = decend.view.gridsize
	local floorsize = decend.game.floor[decend.view.floor].size
	local floorpixelw = floorsize.x * gridsize
	local floorpixelh = floorsize.y * gridsize
	-- draw true background
	love.graphics.clear()
	-- draw grid background
	love.graphics.setColor(0.1, 0.1, 0.1)
	love.graphics.rectangle(
		"fill",
		-cam.x,
		-cam.y,
		floorpixelw,
		floorpixelh
	)
	if decend.view.range then
		-- draw highlighted range
		love.graphics.setColor(0.7, 0.3, 0.3)
		local min = decend:tile2pixel(decend.view.range.min)
		local max = decend:tile2pixel(decend.view.range.max)
		love.graphics.rectangle(
			"fill",
			min.x,
			min.y,
			max.x - min.x,
			max.y - min.y)
	end
	local scrw, scrh = love.graphics.getDimensions()
	-- draw vertical grid lines
	love.graphics.setColor(0, 0, 0)
	local x = decend.util.align(cam.x, gridsize)
	while x < floorpixelw and x < scrw + cam.x do
		local sx = x - cam.x
		love.graphics.line(sx, 0, sx, scrh)
		x = x + gridsize
	end
	-- draw horizontal grid lines
	local y = decend.util.align(cam.y, gridsize)
	while y < floorpixelh and y < scrh + cam.y do
		local sy = y - cam.y
		love.graphics.line(0, sy, scrw, sy)
		y = y + gridsize
	end
	-- draw objects
	for _, o in ipairs(decend.game.floor[decend.view.floor].obj) do
		-- TODO: if this creature is the one whose plan is getting edited rn, show that somehow (check the .creature field of the top of planstack)
		love.graphics.setColor(decend:objcolor(o))
		local pos = decend:tile2pixel(decend.anim:objpos(o))
		local osize = gridsize*4/7
		local obdr = (gridsize - osize)/2
		love.graphics.rectangle(
			"fill",
			pos.x + obdr,
			pos.y + obdr,
			osize,
			osize)
		-- draw hp bar
		local hw = gridsize*3/4
		local hh = obdr*3/4
		local hx = pos.x + (gridsize - hw)/2
		local hy = pos.y + obdr + osize + (obdr - hh)/2
		local hv = decend.util.clamp(0,1,o.st.hp.v/o.st.hp.max)
		local ho = decend.util.clamp(0,1,(o.st.hp.old or o.st.hp.v)/o.st.hp.max)
		if ho > hv then
			-- took damage
			love.graphics.setColor(1, 0, 0)
			love.graphics.rectangle("fill", hx, hy, hw*ho, hh)
			love.graphics.setColor(0, 1, 0)
			love.graphics.rectangle("fill", hx, hy, hw*hv, hh)
		else
			-- gained health
			love.graphics.setColor(0, 1, 1)
			love.graphics.rectangle("fill", hx, hy, hw*hv, hh)
			love.graphics.setColor(0, 1, 0)
			love.graphics.rectangle("fill", hx, hy, hw*ho, hh)
		end
		-- draw outline
		love.graphics.setColor(0, 1, 0)
		love.graphics.rectangle("line", hx, hy, hw, hh)
	end
	-- draw items
	love.graphics.setColor(1,1,1)
	for _, itm in ipairs(decend.game.floor[decend.view.floor].item) do
		local pos = decend:tile2pixel(itm.pos)
		love.graphics.print(itm.t, pos.x, pos.y)
	end
	-- draw tiles
	local floordata = decend.game.floor[decend.view.floor]
	local tilespan = floorsize.x
	for k,v in pairs(floordata.tile) do
		local tpos = { x = k % tilespan, y = math.floor(k / tilespan) }
		local ppos = decend:tile2pixel(tpos)
		love.graphics.print(decend:tilestr(v), ppos.x, ppos.y)
	end
	-- draw msgs
	decend.draw.float_msgs(decend)
	-- draw message log
	decend.draw.msg_log(decend)
	-- draw menus
	decend.draw.menu(decend)
	-- show info about selected creature/tile/item
	decend.draw.selinfo(decend)
	love.graphics.print(love.timer.getFPS().." fps", 7, scrh - 32)
	-- draw current floor info
	-- TODO: say how many turns since this floor was created?
	love.graphics.print("floor "..decend.view.floor.." ["..floordata.area.."], round "..decend.game.round, 3, scrh - 15)
	decend.anim:update()
end

local function do_mouse(pt, button, dx, dy)
	local view = decend.view
	assert(pt)
	
	view.log_scroll_targ = nil
	if decend:menumousemoved(pt) then
		-- do nothing
	elseif
		decend.util.inrect(view.log_scroll_bar_rect, pt) and
		love.mouse.isDown(1)
	then
		local r = view.log_scroll_bar_rect
		local h = r.max.y - r.min.y
		view.log_scroll_targ = (pt.y - r.min.y)/h
	else
		if love.mouse.isDown(1) then
			decend.util.shiftpt(view.pos, -dx, -dy)
		end
		view.sel = decend:pixel2tile(pt)
		view.selobj = decend:ontile(view.floor, view.sel)
	end

end

function love.mousemoved(x, y, dx, dy, istouch)
	local pt = {x = x, y = y}

	do_mouse(pt, 0, dx, dy)
end

function love.mousepressed(x, y, button, istouch, presses)
	local view = decend.view
	local pt = {x = x, y = y}
	if love.keyboard.isDown("lshift") then
		button = button + 1
	end
	if love.keyboard.isDown("lctrl") then
		button = button + 2
	end
	if view.menu then
		if view.menuwin and decend.util.inrect(view.menuwin.rect, pt) then
			if decend.util.inrect(view.menuwin.btn[view.menupos], pt) then
				decend:menuselect(button)
			end
		elseif button == 2 then
			view.menu.func(decend, decend:pixel2tile(pt))
		end
	end
	decend:track("ui.click."..button)
	do_mouse(pt, button, 0, 0)
end

function love.mousereleased( x, y, button, istouch, presses )
	do_mouse({ x = x, y = y }, -button, 0, 0)
end

-- TODO: when zooming, zoom in on the cursor, not toward (0,0)
function love.wheelmoved(x, y)
	local mousep =
		{ x = love.mouse.getX(), y = love.mouse.getY() }
	local scrolly = y*decend.conf.scroll_sens
	if
		decend.view.menuwin and 
		decend.util.inrect(decend.view.menuwin.rect, mousep)
	then
		decend.view.menu.scroll = decend.view.menu.scroll + y
		decend:track("ui.menu_scroll")
	elseif decend.util.inrect(decend.view.log_rect, mousep) then
		decend.view.log_scroll = (decend.view.log_scroll or 0) + scrolly
		decend:track("ui.log_scroll")
	else
		decend.view.gridsize = decend.view.gridsize +
			y*decend.conf.zoom_sens
		decend:track("ui.zoom")
	end
end

local cheat_code_buf = ""
function love.keypressed(key, scancode, isrepeat)
	if isrepeat then
		return
	end
	local view = decend.view
	-- cheat codes, mostly for debugging
	-- TODO: how do cheat codes interact with replay system?  either make cheat code activation stored in replay, or make it so you can't save a replay if you use cheat codes.
	if love.keyboard.isDown("rshift") and string.len(key) == 1 then
		cheat_code_buf = cheat_code_buf .. key
		local s = cheat_code_buf
		local didcheat = true
		if s == "die" then
			decend.game.player.st.hp.v = -1
		elseif s == "open" then
			-- force open all exits, perform() checks the
			-- game.cheat.open field so we don't need to do anything here
		elseif s == "heal" then
			local st = decend.game.player.st
			st.hp.v = st.hp.max
			st.mana.v = st.mana.max
		else
			didcheat = false
		end
		if didcheat then
			decend:msg("cheat code activated: "..s)
			decend.game.cheat[s] = true
			cheat_code_buf = ""
		end
		return
	else
		cheat_code_buf = ""
	end
	decend:track("ui.key."..key)
	do
		local cyclini = require("lib.cyclini")
		if key == "f3" then
			decend:msg("saving game...")
			local f, err = love.filesystem.newFile("quicksave.ini", "w")
			if err then error(err) end
			local rng = decend.game.rng
			decend.game.rng = rng:getState()
			local str = cyclini.marshal(decend.game)
			decend.game.rng = rng
			local ok, err = f:write(str)
			if not ok then error(err) end
			f:close()
			decend:msg("game saved!")
		elseif key == "f4" then
			decend:msg("loading quicksave...")
			local str, err = love.filesystem.read("quicksave.ini")
			if str == nil then
				decend:msg("cannot load quicksave: "..err)
			else
				local game, err = cyclini.unmarshal(str)
				if game == nil then error(err) end
				local rng = love.math.newRandomGenerator(game.seed)
				rng:setState(game.rng)
				game.rng = rng
				decend.game = game
				decend.view.planstack = {}
				decend:menuplan(decend.game.player)
				decend.view.floor = decend.game.player.floor
				decend:msg("quicksave loaded!")
			end
		end
	end
		
	if decend.view.menu then
		local mpos = view.menupos
		if key == "up" then
			mpos = mpos - 1
		elseif key == "down" then
			mpos = mpos + 1
		elseif key == "s" then
			local top = view:plantop()
			if top and top.creature and top.creature.pos then
				view.menu.func(decend, top.creature.pos)
			end
		end
		-- TODO: keyboard shortcut to target player ("p"?)
		if view.menu.item[mpos] then
			decend:menuhover(mpos)
			if key == "return" then
				decend:menuselect(2)
				return
			end
		end
	end
end
