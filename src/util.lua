-- file for functions that do not affect global state

-- TODO: add the function i wrote a while back that does a graph clone, handling circular refrences and such.  that would be helpful for taking snapshots of the game state.

local util = {}
function util.shiftpt(pt, x, y)
	pt.x = pt.x + x
	pt.y = pt.y + y
end

function util.align(v, to)
	return math.floor(v / to) * to
end

function util.distance(a, b)
	return math.max(math.abs(a.x-b.x), math.abs(a.y-b.y))
end

-- make a rectagle centered at the given point
function util.radius(c, r)		
	return {
		min = {
			x = c.x - r,
			y = c.y - r,
		},
		max = {
			x = c.x + r + 1,
			y = c.y + r + 1,
		},
	}
end

function util.clamp(min, max, v)
	return math.max(min, math.min(max, v))
end

function util.ptclamp(r, pt, margin)
	margin = margin or 1
	return {
		x = util.clamp(r.min.x, r.max.x-margin, pt.x),
		y = util.clamp(r.min.y, r.max.y-margin, pt.y),
	}
end

function util.rectclamp(bounds, r)
	return {
		min = util.ptclamp(bounds, r.min),
		max = util.ptclamp(bounds, r.max, 0),
	}
end

-- return the 4 points in the corners of the rect.
-- you may mutate the returned points without causing side effects.
function util.corners(r)
	return {
		{x = r.min.x, y = r.min.y},
		{x = r.max.x-1, y = r.min.y},
		{x = r.min.x, y = r.max.y-1},
		{x = r.max.x-1, y = r.max.y-1},
	}
end

function util.inrect(r, p)
	if r == nil then return false end
	return r.min.x <= p.x and p.x < r.max.x and
		r.min.y <= p.y and p.y < r.max.y
end

-- return only the points that are farthest away for pos
function util.filter_farthest(pos, inlist)
	local ptlist = {}
	local maxdist = 0
	for _,pt in ipairs(inlist) do
		local dist = util.distance(pt, pos)
		if dist > maxdist then
			ptlist = {}
			maxdist = dist
		end
		if dist == maxdist then
			table.insert(ptlist, pt)
		end
	end
	return ptlist
end

-- create a new table with the keys in the argument tables.
-- with one argument it makes a shallow copy
function util.merge(...)
	local r = {}
	for i = 1,select('#',...) do
		for k,v in pairs(select(i,...)) do
			if r[k] == nil then
				r[k] = v
			end
		end
	end
	return r
end

function util.keys(tbl)
	local r = {}
	for k,_ in pairs(tbl) do
		table.insert(r, k)
	end
	return r
end

function util.pickrandom(rand, list)
	return list[rand:random(#list)]
end

function util.random_exclude(rand, min, max, exclude)
	local rand = rand or math.random
	local n = rand:random(min, max-#exclude)
	table.sort(exclude)
	for _, ex in ipairs(exclude) do
		if n >= ex then
			n = n + 1
		end
	end
	return n
end

function util.random_distinct(rand, min, max, n)
	local r = {}
	for i = 1,n do
		table.insert(r, util.random_exclude(rand, min, max, util.merge(r)))
	end
	return r
end

-- used to determine the level of stat changing tiles
function util.rand_tile_lvl(rand)
	local n = 0
	n = n + rand:random(-1, 2)
	n = n + rand:random(-1, 2)
	n = n + rand:random(-2, 1)
	if n == 0 then n = 1 end
	return n
end

function util.rand_pos(rng, size)
	return {
		x = rng:random(0, size.x-1),
		y = rng:random(0, size.y-1),
	}
end

-- return a random point along the rectangle's edge
function util.rand_edge_pos(rng, r)
	local clist = util.corners(r)
	local i = rng:random(4)
	local dx = r.max.x-r.min.x
	local dy = r.max.y-r.min.y
	local p = clist[i]
	--[[ edge positions & ranges
		1>>>2
		^   v
		^   v
		^   v
		3<<<4
	]]
	if i == 1 then
		local o = rng:random(dx)-1
		return { x = p.x + o, y = p.y }
	elseif i == 2 then
		local o = rng:random(dy)-1
		return { x = p.x, y = p.y + o }
	elseif i == 3 then
		local o = rng:random(dx)-1
		return { x = p.x, y = p.y - o }
	else
		local o = rng:random(dy)-1
		return { x = p.x - o, y = p.y }
	end
end

function util.isfaster(o1, o2)
	local s1, s2 = o1.st.spd.v, o2.st.spd.v
	-- table.sort seems to compare entries to themselves sometimes.. 
	if o1 == o2 then return false end
	-- table.sort is not stable,
	-- so we use this to make sure sorting is detirministic
	if s1 == s2 then
		assert(not (o1.id == o2.id), "object id is duplicated")
		return o1.id < o2.id
	end
	return s1 > s2
end

function util.pos2idx(pos, span)
	return pos.x + pos.y*span
end

function util.idx2pos(idx, span)
	return { x = idx % span, y = math.floor(idx / span) }
end

-- remove from a list the first element that is equal
function util.delete(list, elem)
	for k,v in ipairs(list) do
		if v == elem then
			table.remove(list, k)
			return true
		end
	end
	return false
end

function util.map_delete(list, func)
	local i = 1
	while list[i] ~= nil do
		local new = func(list[i])
		if new == nil then
			table.remove(list, i)
		else
			list[i] = new
			i = i + 1
		end
	end
end

-- remove duplicate items
function util.dedup(list)
	local tbl = {}
	local n = 0
	util.map_delete(list, function(elem)
		if tbl[elem] then
			n = n + 1
			return nil
		end
		tbl[elem] = true
		return elem
	end)
	return n
end

-- heal a bar stat, mainly used by the "aid" action
function util.healstat(s, x)
	local empty = s.max - s.v
	local amount = math.min(math.ceil(x or empty/2), empty)
	if s.v < 0 then amount = 0 end
	s.v = s.v + amount
	return amount
end

-- permenatnly modify a stat
function util.modstat(st, change)
	local oldst = 0
	st.base = math.max(0, st.base + change)
	if st.max then
		oldst = st.max
		st.max = st.base
	else
		oldst = st.v
		st.v = st.base
	end
	return st.base, oldst
end

function util.neighbors(pos, bounds)
	local r = {}
	for x = -1,1 do
		for y = -1,1 do
			np = { x = x + pos.x, y = y + pos.y }
			if util.inrect(bounds, np) then
				table.insert(r, np)
			end
		end
	end
	return r
end

function util.floorbounds(floor)
	return {min={x=0,y=0},max=floor.size}
end

function util.has_status(obj, name)
	for i,s in ipairs(obj.status) do
		if s.t == name then
			return i
		end
	end
	return false
end

function util.apply_status(obj, status)
	if status == nil then
	elseif status.t then
		local oldidx = util.has_status(obj, status.t)
		if oldidx then
			obj.status[oldidx].d = obj.status[oldidx].d + status.d
		else
			table.insert(obj.status, {t = status.t, d = status.d})
		end
	else
		for _,s in ipairs(status) do
			util.apply_status(obj, s)
		end
	end
end

function util.show_amounts(prefix, suffix, sep, ifempty, list)
	local str = prefix
	local empty = true
	for _,v in ipairs(list) do
		if v[1] ~= 0 then
			if not empty then
				str = str .. sep
			end
			str = str .. v[1] .. " " .. v[2]
			empty = false
		end
	end
	if empty then
		return ifempty
	end
	return str .. suffix
end

-- try to position close enough to attack, but not too close
function util.poking_pos(rng, opos, targ, rch, mov, bounds)
	local trect = util.radius(targ, rch)
	local orect = util.radius(opos, mov-1)
	local arect = util.rectclamp(util.rectclamp(orect, trect), bounds)
	-- TODO: clamp to within level bounds
	if arect.min.x < arect.max.x and arect.min.y < arect.max.y then
		-- choose the farthest away corner.
		-- if there is a tie, choose randomly
		return util.pickrandom(
			rng, util.filter_farthest(targ, util.corners(arect)))
	else
		return util.ptclamp(orect, targ)
	end
end

function util.signfmt(n)
	if n >= 0 then return "+"..n end
	return tostring(n)
end

function util.knows_spell(o, name)
	for _, sp in ipairs(o.spell) do
		if sp.t == name then
			return sp.exp, sp
		end
	end
	return 0, nil
end

-- exponent based S curve, the probability never reaches 100% or 0%
function util.scurve(n)
	local absn = math.abs(n)
	return -(2^(-absn)-1)/2*n/absn+0.5
end

-- TODO: use this with m > 1 to calculate flinging creatures?
function util.interp_pos(src, dst, m)
	if src == nil then return dst end
	assert(src.x)
	assert(dst.x)
	return {
		x = src.x + (dst.x-src.x)*m,
		y = src.y + (dst.y-src.y)*m,
	}
end

function util.round_pos(pt)
	return {
		x = math.floor(pt.x + 0.5),
		y = math.floor(pt.y + 0.5),
	}
end

-- TODO: version of ipairs that behaves when you remove elements (if the existing version doesn't do that already)

return util
