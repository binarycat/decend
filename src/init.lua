-- TODO: could track statistics such as damage healed and stuff, and use them to give hints (such as aiding a defended creature to heal them)
-- IDEA: when the stat boost tile is consumed, spawn a new one and an unfriendly creature
-- TODO: when traveling across floors, make the stairs match up
-- TODO: give the ai counterplay to any simple strategy the player can do.  it's a rougelike, it's meant to be hard.
-- TODO: lots of convienence features to increase game speed
-- TODO: fix a bunch of random crashes
-- TODO: maybe have an "expected position" when planning moves, so attack range shows more accuratly what you can attack?
-- IDEA: a seperate list of tips for each area, so you get more basic hints when you die earlier.  also helps prevent giving hints about monster types you aren't facing.
-- TODO: allow hovering over tiles for more info
-- TODO: when hovering creatures to get info, print the stats in different colors depening on if they are above or below your own, and by how much.
-- TODO: don't allow creatures to walk out of the level bounds.
-- TODO: small chance of a tile that teaches you a spell spawning instead of one that affects your stats.
-- TODO: implement replay system, then use it to smoothly animate each round.
-- BUG: when ordering a creature that is faster than you, if it is the first turn, you sometimes do a different action instead of ordering.
-- TODO: creatures lose affinity every round they are on a different floor than the player.  this applies to friendly and unfriendly creatures.
-- TODO: remove action priority
-- TODO: some mechanic to make gains to the ACT stat more steady, maybe like ~1 point per area.
-- TODO: "lunge attack" that deals less damage but allows you to move and attack in one action.  this is automatically triggered when doing the "attack" action if you are outside the normal attack range but still fairly close.  this would help prevent the thing that happens sometimes with fast short-range attackers where they get stuck moving to your previous position.
-- TODO: don't show statistics apon death unless the player clicks the "show statistics" button at the top of the game over menu


-- REMEMBER: avoid using pairs and rng together, the order of "pairs" is not defined, so this may cause replay desyncs across platforms.  honestly just avoid using pairs() at all.

return {
	-- affects rendering, and also what user input (mouse, keyboard) does.
	view = {
		pos = { x = 0, y = 0 }, -- camera pan offset
		floor = 1,
		sel = { x = 0, y = 0 }, -- selected tile
		selobj = nil, -- selected/hovered object
		range = nil, -- highlighted range
		menupos = 1,
		menu = nil,
		menuwin = nil, -- menu window
		gridsize = 32,
		texth = 20, -- text line height (DEPRICATED)
		textc = {1, 1, 1, 1}, -- default text color
		planstack = {},
		log_scroll = nil,
		log_scroll_targ = nil,
		log_scroll_bar_rect = { min = { x = 0, y = 0 }, max = { x = 0, y = 0 }},
		plantop = function(view)
			return view.planstack[#view.planstack]
		end,
		planstep = function(view)
			local top = view:plantop()
			return top.plan[top.step]
		end,
		plancancel = function(view)
			local top = view:plantop()
			top.plan[top.step] = nil
		end,
	},
	
	conf = {
		scroll_sens = 12,
		zoom_sens = 1,
		-- tip history timeout paramaters, detirmines how often to repeat hints
		tiphist = {
			-- maximum timeout a tip can have (in rounds)
			max_timeout = 6,
			-- amount to decrease a tips timeout by when
			-- the player meets the conditions for it,
			-- but it is not displayed
			dec_timeout = 2,
			-- initial timeout for a hint after displaying it
			ret_timeout = 4,
		},
		scroll_bar_width = 15,
		-- how fast a scroll bar moves towards the mouse
		scroll_bar_speed = 10,
	},
	
	util = require("src.util"),
	data = require("src.data"),
	menu = require("src.menu"),
	demo = require("src.demo"),
	tips = require("src.tips"),
	draw = require("src.draw"),
	logs = require("src.logs"),
	anim = require("src.anim"),
	
	-- data that is reset on death.
	game = nil,

	-- run once at game load
	init = function(decend)
		local alist = {}
		for k,_ in pairs(decend.data.attr) do
			table.insert(alist, k)
		end
		table.sort(alist)
		decend.data.attr_list = alist
		decend.tips:init()
	end,

	-- run at start of new game
	embark = function(decend, seed)
		decend.logs.data.full = {}
		decend.game = {
			-- each floor of the dungeon
			floor = {},
			-- every object/creatue, by id
			obj = {},
			rng = love.math.newRandomGenerator(),
			-- a round consists of a number of turns, each turn allows each
			-- creature to take an action if they have action points left for
			-- the round
			turn = 0,
			round = 0,
			-- each object has a unique id
			nextid = 1,
			seed = seed or os.time(),
			statistic = {},
			cheat = {}, -- cheat codes activated
		}
		decend:msg("= YOUR QUEST BEGINS ANEW =")
		decend:msg("seed: "..decend.game.seed)
		decend.game.rng:setSeed(decend.game.seed)
		decend:newfloor(1)
		decend.game.player = decend:spawn(1, "player")
		decend.game.active = decend.game.player
		decend.game.player.st.aff.v = 100
		if decend.demo.mode == nil or decend.demo.mode == "record" then
			decend.demo.record(decend)
		end
	end,

	newfloor = function(decend, floorno)
		local size = 12 + floorno*4
		local floor = {
			size = { x = size, y = size },
			obj = {},
			tile = {}, -- top left of map is tile 0
			item = {},
			created = decend.game.round,
			spawn_count = (floorno-1)%3 + 1,
			exit_unlocked = false,
			act_tile_spawned = false,
		}
		local floorinfo = decend.data.floor[floorno]
		if floorinfo and floorinfo.area then
			floor.area = floorinfo.area
		else
			floor.area = decend.game.floor[floorno-1].area
		end
		assert(floor.area, "new floor has nil area")
		local ntiles = floor.size.x * floor.size.y
		local tiles = decend.util.random_distinct(
			decend.game.rng, 0, ntiles-1, 2)
		floor.tile[tiles[1]] = "ustair"
		floor.tile[tiles[2]] = "dstair"
		decend.game.floor[floorno] = floor
		decend:spawn_stat_tile(floorno)
		local firstc = decend:spawn(floorno)
		if floorno ~= 1 then
			if floor.spawn_count > 1 then
				decend:spawn(floorno)
			end
			if floor.spawn_count > 2 then
				decend:spawn(floorno)
			end
		else
			-- as a combat tutorial, the first creature on
			-- the first floor has extra low affinity.
			firstc.st.aff.v = firstc.st.aff.v - 30
		end
		local itm = decend:create_item(
			decend.util.pickrandom(
				decend.game.rng,
				decend.data.area[floor.area].item),
			decend.util.rand_pos(decend.game.rng, floor.size))
		table.insert(floor.item, itm)
		decend:displace(floorno)
	end,
	
	create_item = function(decend, t, pos)
		local itm = { t = t, pos = pos }
		itm.charge = decend.data.item[t].charge
		return itm
	end,
	
	spawn_stat_tile = function(decend, floorno)
		local rng = decend.game.rng
		local floor = decend.game.floor[floorno]
		local tiletbl = floor.tile
		local ntiles = floor.size.x * floor.size.y
		local pos = decend.util.random_exclude(
			rng, 0, ntiles-1, decend.util.keys(tiletbl))
		assert(tiletbl[pos] == nil, "tried to spawn a tile on top of another!")
		-- we expect the player to gain a point in ACT about once every 3 floors.
		-- if they fall behind, there's a chance to spawn an ACT+ tile.
		-- only one ACT+ tile can spawn per floor.
		if
			-- we need to check that the player exists bc the first floor is
			-- generated before the player is spawned.
			decend.game.player ~= nil and
			floor.act_tile_spawned == false and
			decend.game.player.st.act.v < (floorno-1)/3 + 2 and
			rng:random() < 0.2
		then
			tiletbl[pos] = { t = "stat", st = "act", lvl = 1 }
			floor.act_tile_spawned = true
		else
			tiletbl[pos] = {
				t = "stat",
				st = decend.util.pickrandom(rng, decend.data.st_tile_list),
				lvl = decend.util.rand_tile_lvl(rng),
			}
		end
	end,
	
	spawn = function(decend, floorno, ct, domsg, at_edge)
		local alist = decend.data.attr_list
		local floordata = decend.game.floor[floorno]
		local spawnlist = decend.data.area[floordata.area].spawn
		local rng = decend.game.rng
		ct = ct or decend.util.pickrandom(rng, spawnlist)
		local cdata = decend.data.creature[ct]
		assert(cdata, "unknown creature type: "..ct)
		-- TODO: should we be giving the player attributes??
		local c = {
			id = decend.game.nextid,
			t = ct,
			floor = floorno,
			native_floor = floorno,
			has_died = false,
			st = {},
			plan = {},
			attr = alist[rng:random(#alist)],
			attack = cdata.attack,
			effect = decend.util.merge(decend.data.creature[ct].effect),
			spell = {}, -- list of know spells
			status = {}, -- active status effects
			pos = nil,
			aggro = nil, -- last creature to hit this one
		}
		if at_edge then
			c.pos = decend.util.rand_edge_pos(rng, decend.util.floorbounds(floordata))
		else
			c.pos = decend.util.rand_pos(rng, floordata.size)
		end
		decend.game.nextid = decend.game.nextid + 1
		local attr = decend.data.attr[c.attr]
		-- set stats
		for k, v in pairs(decend.data.st) do
			local st = { v = decend.data.creature[ct].st[k] or v.d }
			st.v = math.ceil(st.v * (attr[k] or 1))
			if v.isbar then
				st.max = st.v
			end
			st.base = st.v
			c.st[k] = st
		end
		-- every 5 turns you stay on a floor, the affinity of new creatures decreases
		local time_boost = (decend.game.round-floordata.created)/5
		-- teach spells
		for _, sp in ipairs(decend.data.creature[ct].spell or {}) do
			if sp.prob == nil or rng:random() < sp.prob then
				decend:teachspell(c, sp.t, sp.exp or 1)
			end
		end
		if cdata.item then
			c.item = decend:create_item(cdata.item)
		end
		-- TODO: make it so attributes can affect initial affinity?
		c.st.aff.v =
			decend.game.rng:random(-2*(c.st.hp.v+c.st.int.v), -c.st.will.v) -
				2*floorno - math.floor(3*time_boost)
		table.insert(floordata.obj, c)
		if domsg then
			decend:msgon(c)
			decend:msgsvo(c, "spawned!")
		end
		return c
	end,

	-- TODO: animate this instead of doing it all in 1 frame
	-- do all the logic for a single round
	update = function(decend, floorno)
		floorno = floorno or decend.game.active.floor
		local game = decend.game
		local floor = game.floor[floorno]

		game.round = game.round + 1

		decend.logs:beginround()
		decend:msg("= ROUND "..game.round.." BEGIN =")

		decend.anim.turn = {}
		-- TODO: should the spawn clock be seperate for each floor? (based on the .created field)
		if decend.game.round % 5 == 0 then
			for i = 1,floor.spawn_count do
				decend:spawn(floorno, nil, true, true)
			end
			floor.spawn_count = floor.spawn_count + 1
		end
		
		for _,o in ipairs(floor.obj) do
			game.active = o
			decend:think()
			o.st.hp.old = o.st.hp.v
		end
		
		local continue = true
		while continue do
			continue = false
			game.turn = game.turn + 1
			decend:anim_capture(floor)
			decend:msg("== turn "..game.turn.." ==")
			-- failsafe, should not be needed
			local ndup = decend.util.dedup(floor.obj)
			if ndup > 0 then print("warning: "..ndup.." duplicate objs") end
			table.sort(floor.obj, decend:actsfirst(game.turn))
			for _,o in ipairs(floor.obj) do
				game.active = o
				continue = decend:perform() or continue
				-- never allow a creature to move outside the floor.
				o.pos = decend.util.ptclamp(
					decend.util.floorbounds(floor), o.pos)
			end
		end
		decend:msg("= ROUND "..game.round.." END =")
		decend:displace(floorno)
		
		-- yes, tiles only activate at the end of a round. honestly might keep this, it seems more interesting.
		for _,o in ipairs(floor.obj) do
			local tile, tilepos = decend:tileunder(o)
			if tile and type(tile) == "table" and tile.t == "stat" then
				local attrmul = decend.data.attr[o.attr][tile.st] or 1
				local newst, oldst =
					decend.util.modstat(o.st[tile.st], 
						math.ceil(tile.lvl * attrmul))
				decend:msgon(o)
				decend:msg("a tile modified "..
					decend:describe(o).."'s "..
					tile.st.. " ("..oldst.." -> "..
					newst..")")
				decend:trackp("tile.stat."..tile.st)
				decend:spawn_stat_tile(floorno)
				floor.tile[tilepos] = nil
			end
			-- statuses doing damage/wearing off
			decend.util.map_delete(o.status, function(s)
				local sdata = decend.data.status[s.t]
				if sdata and sdata.dot then
					local dmg = math.ceil(o.st.hp.max / sdata.dot)
					decend:msgsvo(o, "took "..dmg.." damage from "..s.t)
					decend:hurt(o, dmg, "status", s.t)
				end
				s.d = s.d - 1
				if s.d < 0 then
					decend:msgsvo(o, "{is|are} no longer afflicted by", s.t)
					return nil
				end
				return s
			end)
			decend:refreshstat(o)
		end
		
		-- creatures dying
		decend.util.map_delete(floor.obj, function(o)
			if o.item and o.item.charge and o.item.charge <= 0 then
				decend:msgon(o)
				decend:msgsvo(o.item, "broke!")
				o.item = nil
			end
			if o.st.hp.v >= 0 then return o end
			-- TODO: somewhere in here creatures should drop their items.
			if not o.has_died then
				decend:msgsvo(o, "perished!")
				o.has_died = true
				if o.native_floor == floorno then
					if not floor.exit_unlocked then
						decend:msgon(o)
						decend:msg("exit unlocked!")
						floor.exit_unlocked = true
					end
				end
			end
			if o.st.hp.v < -o.st.hp.max then
				decend:msgon(o)
				decend:msgsvo(o, "disintegrate{s|}!")
				return nil
			end
			return o
		end)
		
		for _,o in ipairs(floor.obj) do
			-- friendly creatrures gain affinity while at max hp.
			if o.st.aff.v > 0 and o.st.hp.v >= o.st.hp.max then
				decend:endear(o, 1)
			end
			-- track targets one last time so they show properly on
			-- the menu screen
			for _,a in ipairs(o.plan) do
				if a.obj then
					a.targ = a.obj.pos
				end
			end
			if o.guard then
				if o.guard.st.hp.v < 0 or
					o.guard.floor ~= o.floor
				then
					o.guard = nil
					decend:track("break_guard.floor")
				end
			end
		end

		game.turn = 0
		game.active = game.player
		if decend.game.player.st.hp.v < 0 then
			decend:menuopen(decend.menu.dead)
			decend:msg("= GAME OVER =")
			local tip = decend.tips:choose(
				decend.game.statistic,
				decend.conf.tiphist)
			decend:msg("tip: "..tip.text)
			decend:showstatistics()
			local ok, err = pcall(decend.tips.save, decend.tips)
			if not ok then print("WARNING: could not save tip history: "..err) end
		elseif decend.demo.mode == "replay" then
			decend:menuopen(decend.menu.replay)
		else
			decend:menuplan(game.player)
		end
		decend.view.log_scroll = nil
	end,

	-- displace objects out of each other
	displace = function(decend, floorno)
		local floor = decend.game.floor[floorno]
		for _,o in ipairs(floor.obj) do
			local objs = decend:ontile(floorno, o.pos, true)
			if #objs > 1 then
				local rng = decend.game.rng
				local util = decend.util
				local poslist = util.neighbors(o.pos,util.floorbounds(floor))
				-- displace the slowest obj
				-- without this repeatedly moving to a creature will
				-- cause you and said creature to wander randomly
				local movobj = objs[#objs]
				local newpos = util.pickrandom(rng, poslist)
				decend:msgsvo(objs[1], "displaced", movobj)
				movobj.pos = newpos
			end
		end
	end,

	-- perform an action
	perform = function(decend)
		local game = decend.game
		local floor = game.floor[game.active.floor]
		local o = game.active -- object performing the action
		local a = o.plan[game.turn] -- the action to be performed
		-- the object is unable to act any further this round
		-- return value is used to detirmine when to exit the update loop.
		if o.st.act.v < game.turn or o.st.hp.v < 0 then
			return false
		end
		if a == nil then
			-- object was just spawned, and has no action.
			-- next round, it will start choosing actions.
			o.plan[game.turn] = { t = "auto" }
			return true
		end
		decend:msg("=== "..decend:describe(o).." ===")
		if o == decend.game.player then
			local suf = ".other"
			if o == a.obj then
				suf = ".self"
			elseif a.obj == nil then
				suf = ".tile"
			end
			decend:track("act."..a.t..suf)
		end
		-- TODO: if aff is low enough and player is within attack range, attack player regardless of health?
		-- TODO: if low health and faster than the creature attacking it, run away?
		-- TODO: fast long range attackers should try and space out their opponent during a fight?
		-- TODO: if has no item and there is an item within range, pick it up
		-- TODO: reduce bloodcasting propability (inverse function maybe? (1/x)
		if a.t == "auto" then
			if o.st.hp.v < o.st.hp.max/4 and o.st.mana.v < o.st.mana.max/2 then
				a = {
					t = "aid",
					obj = o,
				}
			elseif o.aggro then
				a = {
					t = "attack",
					obj = o.aggro,
				}
			elseif o.st.hp.v < o.st.hp.max/2 then
				a = { t = "aid", obj = o }
			elseif o.st.aff.v < -10 then
				local spell = decend.util.pickrandom(game.rng, o.spell)
				-- TODO: only cast negitive spells
				-- if the affinity is low enough, it will cast a spell using hp
				-- TODO: if it is a benifitial spell, cast it on the creature the player is attacking instead.
				if spell and
					decend.game.rng:random() < o.st.mana.v/o.st.mana.max and
					decend.util.distance(o.pos, decend.game.player.pos) <=
						decend:spellrange(spell, o) and
					(decend:spellcost(spell) <= o.st.mana.v or
						o.st.aff.v < 3*(o.st.hp.v-o.st.hp.max))
				then
					local spelldata = decend.data.spell[spell.t]
					a = {
						t = "cast",
						-- cast helpful spells either on the last creature to
						-- attack the player, or on self.
						-- cast other spells on player
						obj = spelldata.helpful and 
							(game.player.aggro or o) or
							game.player,
						extra = spell.t,
					}
				else
					a = {
						t = "attack",
						obj = game.player,
					}
				end
			else
				a = {
					t = "aid",
					obj = o,
				}
			end
		end
		
		-- TODO: defending a creature increases its affinity by an amount depending on the damage taken.
		-- if we targeted an object, update the target tile to track that object.
		if a.obj then
			a.targ = a.obj.pos
		else
			-- if no object is targeted, try to lock on to one.
			a.obj = decend:ontile(o.floor, a.targ)
		end
		local dist = decend.util.distance(a.targ, o.pos)
		local arange = decend:actionrange(a.t, o)
		if arange and dist > arange then
			-- change the action that is performed, but not the underlying plan
			a = {
				t = "move",
				targ = decend.util.poking_pos(
					decend.game.rng,
					o.pos, a.targ, arange, o.st.mov.v,
					decend.util.floorbounds(decend.game.floor[o.floor])),
			}
			dist = decend.util.distance(a.targ, o.pos)
		end
		-- check if we are blocked from doing the action
		if decend.util.has_status(o, "no_"..a.t) then
			decend:msg("unable to "..a.t.."!")
			return true
		end
		-- do actions
		if a.t == "move" then
			if dist > o.st.mov.v then
				-- should never happen now that the above check exists
				decend:msg("cannot move: too far away")
			else
				decend:msgsvo(o, "move{s|}")
				o.pos = a.targ
				if o.guard then
					decend:track("break_guard.move")
					o.guard = nil
				end
			end
			decend:trackp("didact.move")
		elseif a.t == "attack" then
			if dist > o.st.rch.v then
				decend:msg("attack failed: target is too far away!")
			else
				local t = a.obj
				if not t then
					decend:msg("attack failed: target not found!")
				else
					local effect = nil
					if o.item then
						effect = decend.data.item[o.item.t].attack
						if o.item.charge then
							o.item.charge = o.item.charge - 1
						end
					end
					decend:msgsvo(o, "attack{s|}", t)
					decend:attack(o, t, o.st.atk.v, effect or o.attack)
					decend:trackp("didact.attack")
				end
			end
		elseif a.t == "aid" then
			local t = a.obj
			if not t then
				decend:msg("nothing to aid")
			elseif t.guard then
				decend:msgsvo(o, "heal{s|}", t)
				local hp = decend.util.healstat(t.st.hp)
				local mana = decend.util.healstat(t.st.mana)
				decend:msg(
					decend.util.show_amounts(
						"restored ", "!", " and ",
						"already fully healed!",
						{{hp, "hp"}, {mana, "mana"}}))
				if hp == 0 and mana == 0 then
					if t == o then
						decend:learnfrom(t, o, 8, "teach.self")
					else
						decend:learnfrom(t, o, 4, "teach.other")
						decend:endear(t, math.max(1, o.st.int.v - t.st.will.v), "teach")
					end
					decend:trackp("didact.aid.teach")
				else
					decend:endear(t, hp, "heal")
					decend:trackp("didact.aid.heal")
				end
			else
				decend:msgsvo(o, "defend{s|}", t)
				t.guard = o
				if t ~= o then
					decend:endear(t, math.max(1, o.st.will.v - t.st.will.v), "guard")
				end
				decend:trackp("didact.aid.guard")
			end
		elseif a.t == "order" then
			if not a.obj then
				-- this should never happen
				decend:msg("nothing to order!!!")
			else
				decend:msgon(o)
				decend:msgsvo(o, "order{s|}", a.obj)
				-- every 10 points above 0 halves the chance of disobedience
				local r = (o.st.will.v - a.obj.st.will.v)
				if o == decend.game.player then
					r = r + a.obj.st.aff.v
				end
				if decend.game.rng:random() < decend.util.scurve(r/10) then
					a.obj.plan = a.extra
					a.obj.order = { round = decend.game.round, obj = o }
					decend:trackp("didact.order.obey")
				else
					decend:msgon(a.obj)
					decend:msgsvo(a.obj, "refuse{s|} to listen")
				end
			end
		elseif a.t == "climb" then
			if floor.exit_unlocked or decend.game.cheat.open then
				local tile = decend:tileunder(o)
				local d = ({
						ustair = { v = -1, s = "ascend{s|}", d = "up" },
						dstair = { v =  1, s = "decend{s|}", d = "down" },
				})[tile]
				if d then
					-- TODO: removes the object from the list of object on this floor, which is a problem, as we are currently iterating over that list. (does this _actually_ cause bugs?)
					local newfloor = d.v + o.floor
					decend:msgsvo(o, d.s, "the staircase")
					decend:setfloor(o, newfloor)
					decend:displace(newfloor)
					decend:trackp("didact.climb."..d.d)
				else
					decend:msg("nothing to climb!")
				end
			else
				decend:msg("cannot climb: exit is locked!")
				decend:trackp("didact.climb.locked")
			end
		elseif a.t == "cast" then
			local t = a.obj
			local spell = decend.data.spell[a.extra]
			if spell == nil then error("unknown spell: "..a.extra) end
			local mana = math.min(o.st.mana.v, spell.cost)
			-- casting never fails, but it may kill you in the process.
			local hp = spell.cost - mana
			decend:msgon(o)
			decend:msgsvo(o, "cast{s|} "..a.extra.." on", a.obj,
						  decend.util.show_amounts(
							  " (spent ", ")", ", ", "",
							  {{mana, "mana"}, {hp, "hp"}}))
			
			local cdata = decend.data.creature[a.obj.t]
			if cdata.spell and cdata.spell[1] and cdata.spell[1].t==a.extra then
				-- creatures are immune to the first spell in their spell list,
				-- they will reflexivly counter them if they are the target
				decend:msgon(a.obj)
				decend:msgsvo(
					a.obj,
					"reflexivly unwind{s|} the magic of the",
					a.extra, "spell!")
				decend:track("spell_immune."..a.extra)
			else
				-- do spell effects
				if spell.dmg and a.obj then
					local power
					if a.extra == "decimate" then
						power = math.ceil(a.obj.st.hp.max / 10)
					else
						power = decend:statref(spell.dmg, o)
					end
					assert(type(power) == "number")
					local dmg = decend:attack(o, a.obj, power,
						spell.attack or "magic")
					if spell.lifesteal then
						local hp_healed = decend.util.healstat(o.st.hp,
							dmg * spell.lifesteal)
						decend:msg("recovered "..hp_healed.." hp!")
					end
				end
				if a.extra == "examine" and t then
					decend:msgsvo(t, "know{s|} "..#t.spell.." spell(s)")
					for _, sp in ipairs(t.spell) do
						decend:msg("* "..sp.t.." L"..sp.exp)
					end
					local elist = decend.util.keys(t.effect)
					if #elist > 0 then
						table.sort(elist)
						decend:msg("weaknesses/resitances:")
						for _,ef in ipairs(elist) do
							decend:msg("* "..ef..": x"..t.effect[ef])
						end
					end
					-- TODO? native floor?
					-- TODO? description of creature attribute?
				elseif a.extra == "protect" and t then
					t.guard = o
				elseif t then
					decend:dospellextra(o, t, a.extra)
				end
				decend:trackp("didact.cast."..a.extra)
				decend.util.apply_status(a.obj, spell.status)
				decend:refreshstat(a.obj)
			end
			-- apply costs
			o.st.mana.v = o.st.mana.v - mana
			decend:hurt(o, hp, "bloodcast", a.extra)
		elseif a.t == "grab" then
			local itm = decend:ontile(o.floor, a.targ, false, "item")
			if itm then
				if o.item ~= nil then
					-- drop current item to make room for new one
					decend:msgon(o)
					decend:msgsvo(o, "drop{s|}", o.item)
					decend:putitem(o.floor, o.pos, o.item)
					o.item = nil
				end
				o.item = itm
				itm.pos = nil
				decend:msgon(o)
				decend:msgsvo(o, "pick{s|} up", itm)
				assert(decend.util.delete(
					decend.game.floor[o.floor].item, itm))
				decend:refreshstat(o)
				decend:trackp("didact.grab")
			else
				-- TODO: make it possible to grab an item out of a creatures hands if you pass a check.
				decend:msgon(o)
				decend:msg("nothing to pick up")
			end
		elseif a.t == "throw" then
			local itm = o.item
			o.item = nil
			if itm == nil then
				decend:msg("nothing to throw!")
			else
				decend:refreshstat(o)
				decend:msgsvo(o, "throw{s|}", itm)
				if a.obj then
					decend:trackp("didact.throw.obj."..itm.t)
					if itm.t == "salve" and a.obj.st.hp.v >= 0 then
						a.obj.st.hp.v = a.obj.st.hp.max
						decend:msgon(a.obj)
						decend:msgsvo(a.obj, "{was|were} fully healed")
					elseif a.obj.item == nil and
							decend.game.rng:random(100) < a.obj.st.spd.v then
						decend:msgon(a.obj)
						decend:msgsvo(a.obj, "catch{s|}", itm)
						a.obj.item = itm
						decend:refreshstat(o)
					else
						decend:msgsvo(itm, "hit{s|}", a.obj)
						local iattack = decend.data.item[itm.t].attack
						if iattack then
							decend:attack(o, a.obj, o.st.atk, iattack)
						end
						decend:putitem(o.floor, a.obj.pos, itm)
					end
				else
					decend:trackp("didact.throw.tile."..itm.t)
					decend:msgsvo(itm, "fall{s|} to the ground")
					decend:putitem(o.floor, a.targ, itm)
				end
			end
		else
			error("unknown action type: "..a.t)
		end
		return true
	end,
	
	-- returns the tile under specified object
	-- "object" may also be a table with just floor and pos set
	tileunder = function(decend, o)
		local floor = decend.game.floor[o.floor]
		local idx = decend.util.pos2idx(o.pos, floor.size.x)
		return floor.tile[idx], idx
	end,
	
	setfloor = function(decend, o, floorno)
		if o.floor then
			decend.util.delete(decend.game.floor[o.floor].obj, o)
		end
		if decend.game.floor[floorno] == nil then
			decend:newfloor(floorno)
		end
		o.floor = floorno
		table.insert(decend.game.floor[floorno].obj, o)
		o.plan = {}
	end,
	
	-- extra effects of spells.
	dospellextra = function(decend, o, t, name)
		assert(o and t and name, "nil argument!")
		if name == "reload" then
			local itm = t.item
			if itm == nil then
				decend:msg("no item to reload!")
			elseif itm.charge == nil then
				decend:msg("item does not have ammo!")
			else
				local count = item.reload_count or 0
				local idata = decend.data.item[itm.t]
				local amount = idata.charge/(2^count)
				item.charge = item.charge + amount
				item.reload_count = count + 1
				decend:msg("restored "..amount.." ammo!")
			end
		end
	end,

	attack = function(decend, o, t, dmg, effect)
		if t == nil then
			decend:msg("attack failed: no target")
			return 0
		end
		local hit = t
		local m = 1
		local crit_factor = 1
		local status
		dmg = dmg or o.st.atk.v
		if t.guard then
			hit = t.guard
			m = 2
			if t.guard ~= t then
				decend:msgon(t.guard)
				decend:msgsvo(t.guard, "takes the damage for", t)
				local mana = decend.util.healstat(t.guard.st.mana, dmg)
				if mana > 0 then
					decend:msgon(t.guard)
					decend:msgsvo(t.guard, "recovered "..mana.." mana!")
					decend:track("heal.mana.block", mana)
				end
				-- TERM: defend/guard just means setting t.guard, "block" means actually taking damgage for a creature
				decend:endear(t, dmg, "block")
			end
		end
		local edata = decend.data.effect[effect]
		if edata then
			if edata.crit then crit_factor = edata.crit end
			if edata.status then status = edata.status end
		end
		if decend.util.has_status(hit, "vuln") then
			crit_factor = 2*crit_factor
		end
		if decend.game.rng:random(100) <
				(o.st.int.v - hit.st.int.v)*crit_factor then
			decend:msgon(o)
			decend:msg("critical hit!")
			m = m / 3
		end
		local resist = (hit.effect[effect] or 1) -- weakness/resistances
		m = m / resist
		local def = math.floor(hit.st.def.v*m*((edata or {}).def or 1))
		local dmg = math.max(0, math.ceil(dmg/m) - def)
		if hit.st.hp.v < 0 then -- disection
			decend:learnfrom(o, hit, 2, "disect")
		elseif status then
			local int = hit.st.int.v
			if
				t.guard ~= hit and -- being defended prevents status conditions
				decend.game.rng:random(int/2, int*2) > dmg/m
			then
				decend.util.apply_status(hit, status)
				decend:msgon(hit)
				decend:msg("afflicted with: "..(status.t or "several statuses"))
				decend:track("got_status.combat."..status.t)
			end
		end
		if effect == "fling" then
			local util = decend.util
			local dist = math.floor(dmg*10/hit.st.hp.max)
			local pos = util.ptclamp(
				util.radius(hit.pos, dist),
				util.round_pos(util.interp_pos(o.pos, hit.pos, dist+2)))
			decend:msg("flung "..dist.." tiles!")
			hit.pos = pos
		end
		decend:hurt(hit, dmg, "combat", effect)
		if o == hit then
			decend:endear(hit, -4*dmg, "hit.self")
		elseif o.t == hit.t then
			-- creatures do not like fighing their own kind
			decend:endear(hit, -2*dmg, "hit.alike")
			decend:endear(hit, -dmg, "infighting")
		elseif hit.st.aff.v < 0 then
			decend:endear(hit, -dmg, "hit.other")
		else
			-- TODO: integrate will in here somewhere?
			decend:endear(hit, -dmg/4, "hit")
		end
		t.guard = nil
		t.aggro = o
		-- TODO: effectiveness as a percentage??
		decend:msg("dealt "..dmg.." "..effect.." damage, "..
			resist.."x effective!")
		return dmg
	end,
	
	hurt = function(decend, o, dmg, kind, ty)
		local ty = ty or "other"
		o.st.hp.v = o.st.hp.v - dmg
		if decend.game.player == o then
			decend:track("hurt."..kind.."."..ty, dmg)
			if o.st.hp.v < 0 then
				decend:track("died_to."..kind.."."..ty)
			end
		end
	end,
	
	-- modify affinity
	endear = function(decend, o, aff, method)
		method = method or "other"
		aff = math.floor(aff)
		o.st.aff.v = o.st.aff.v + aff
		if aff > 0 then
			decend:track("endear.gain."..method, aff)
		else
			decend:track("endear.lose."..method, aff)
		end
	end,

	-- start of round logic for creatures
	think = function(decend)
		local game = decend.game
		local o = game.active

		for i = 1,o.st.act.v do
			if o.aggro and o.aggro.has_died then
				o.aggro = nil
			end
			
			-- high int and low aff makes it more likely that
			-- a creature will trust its own actions and doubt orders.
			local unsure_prob = (o.st.int.v - o.st.aff.v)
			if o.order then
				-- a creature will become less sure of its order the
				-- longer it is left with them.
				unsure_prob = unsure_prob + game.round - o.order.round
			else
				unsure_prob = -unsure_prob
			end
			if o ~= game.player and 
				game.rng:random(100) < math.max(1, unsure_prob)
			then
				decend:msgon(o)
				decend:msgsvo(o, "{is|are} unsure what to do!")
				o.order = nil
				o.plan = {}
				decend:track("creature_unsure")
			end
			
			if o.plan[i] then
				if o.plan[i].obj and o.plan[i].obj.floor ~= o.floor then
					o.plan[i].obj = nil
				end
				if o.plan[i].obj == nil then
					o.plan[i].obj = decend:ontile(o.floor, o.plan[i].targ)
				end
			end
			
			if o.st.aff.v > 0 then
				-- if the affinity stat is greater than 10*WILL, then
				-- it decays over time.
				local amount = math.floor(o.st.aff.v/(o.st.will.v*10))
				decend:endear(o, -amount, "decay")
			end
		end
	end,

	teachspell = function(decend, c, name, exp)
		local oexp, sp = decend.util.knows_spell(c, name)
		if sp then
			sp.exp = exp + oexp
		else
			table.insert(c.spell, { t = name, exp = exp })
		end
	end,

	learnfrom = function(decend, pupil, c, div, method)
		method = method or other
		decend:track("learn.count."..method)
		decend:msgsvo(pupil, "{is|are} learning from", c)
		for _,st in ipairs(decend.data.st_learn_list) do
			q = math.ceil(c.st[st].base / div) /
				math.max(1, pupil.st[st].base) *
				(decend.data.attr[pupil.attr][st] or 1) /
				math.max(0.5, (decend.data.attr[c.attr][st] or 1))
				
			local i = math.floor(q)
			if decend.game.rng:random() < (q-i)^(decend.data.st[st].learn_exp or 1) then
				i = i + 1
			end
			if i > 0 then
				local newst, oldst = decend.util.modstat(pupil.st[st], i)
				decend:msgon(pupil)
				decend:msg(st.." +"..i..": "..oldst.." -> "..newst)
			end
		end
		for _,sp in ipairs(c.spell) do
			local expdiff = sp.exp - decend.util.knows_spell(pupil, sp.t)
			local n = pupil.st.int.v*math.max(0.5, expdiff)/div
			if decend.game.rng:random() < decend.util.scurve(n/5-2) then
				decend:teachspell(pupil, sp.t, 1)
				decend:msgon(pupil)
				decend:msg("spell learned: "..sp.t)
				decend:track("learn.spell."..sp.t)
			end
		end
		decend:refreshstat(pupil)
		-- TODO: slowly inherit weaknesses/resistances (this part should not be random)
	end,
	
	refreshstat = function(decend, o, st)
		if st == "aff" then return end
		if st == nil then
			for _,s in ipairs(decend.data.st_list) do
				decend:refreshstat(o, s)
			end
		else
			local imod = 0
			local smod = 1
			if o.item then imod = (decend.data.item[o.item.t].st[st] or 0) end
			for _,s in ipairs(o.status) do
				local sinfo = decend.data.status[s.t]
				if sinfo and sinfo.st and sinfo.st[st] then
					smod = smod * sinfo.st[st]
				end
			end
			local v = math.ceil(o.st[st].base*smod + imod)
			if o.st[st].max then
				o.st[st].max = v
			else
				o.st[st].v = v
			end
		end	
	end,

	actsfirst = function(decend, turn)
		return function(o1, o2)
			local a1, a2 = (o1.plan[turn] or {t="auto"}), (o2.plan[turn] or {t="auto"})
			local pri1 = decend.data.action[a1.t].pri or 0
			local pri2 = decend.data.action[a2.t].pri or 0
			if pri1 == pri2 then return decend.util.isfaster(o1, o2) end
			return pri1 > pri2
		end
	end,

	describe = function(decend, obj, short)
		local s = obj.t
		if obj.charge then
			s = s.." ("..obj.charge..")"
		end
		if obj.attr then
			s = obj.attr.." "..s
		end
		if not short then
			s = "the "..s
		end
		return s
	end,
	
	-- describe one step of a creature's plan
	describe_step = function(decend, act, obj)
		if act == nil then return "nothing" end
		local verb = act.t
		if decend.data.action[verb].r == false then
			return verb
		end
		if verb == "move" then verb = "move to" end
		local targ
		if act.obj then
			if act.obj == obj then
				targ = "self"
			else
				targ = decend:describe(act.obj)
			end
		else
			targ = "("..act.targ.x..","..act.targ.y..")"
		end
		if verb == "cast" then
			return "cast "..act.extra.." on "..targ
		end
		return verb.." "..targ
	end,
	
	msgon = function(decend, o)
		-- the next message will show above this creature
		decend.game.msg_focus = o
	end,

	msg = function(decend, text)
		decend.logs:msg(decend.game.msg_focus, text)
		decend.game.msg_focus = nil
	end,

	msgsvo = function(decend, s, v, o, suffix)
		local str = ""
		local ss, sv, so = s, v, o -- stringified versions
		local plural = false
		suffix = suffix or ""
		if type(s) ~= "string" then
			if decend.game.player == s then
				ss = "you"
				plural = true
			else
				ss = decend:describe(s)
			end
		end
		if plural then
			sv = string.gsub(v, "{%w*|(%w*)}", "%1")
		else
			sv = string.gsub(v, "{(%w*)|%w*}", "%1")
		end
		str = str..ss.." "..sv
		if o then
			if type(o) ~= "string" then
				if s == o then
					if ss == "you" then
						so = "yourself"
					else
						so = "itself"
					end
				elseif decend.game.player == o then
					so = "you"
				else
					so = decend:describe(o)
				end
			end
			--str = str.." "..so
			so = " "..so
		end
		so = so or ""
		decend:msg({
			decend:objcolor(s), ss,
			decend.view.textc, " "..sv,
			decend:objcolor(o), so,
		})
	end,

	ontile = function(decend, floorno, pos, all, ty)
		local r = {}
		ty = ty or "obj"
		if pos == nil then return nil end
		for _,o in ipairs(decend.game.floor[floorno][ty]) do
			if o.pos.x == pos.x and o.pos.y == pos.y then
				if all then
					table.insert(r, o)
				else
					return o
				end
			end
		end
		if all then return r end
	end,

	tile2pixel = function(decend, tilepos)
		return {
			x = math.floor(tilepos.x * decend.view.gridsize - decend.view.pos.x),
			y = math.floor(tilepos.y * decend.view.gridsize - decend.view.pos.y),
		}
	end,

	pixel2tile = function(decend, pixelpos)
		local view = decend.view
		return {
			x = math.floor((pixelpos.x + view.pos.x)/view.gridsize),
			y = math.floor((pixelpos.y + view.pos.y)/view.gridsize),
		}
	end,

	-- open the menu to plan the actions for a creature
	-- if c is nil, resume planning that is already being done.
	menuplan = function(decend, c)
		if c then
			table.insert(
				decend.view.planstack,
				{
					creature = c,
					plan = decend.util.merge(c.plan),
					step = nil,
				}
			)
		end	
		decend:menuopen(decend.menu.plan)
		if not c then
			decend:menuhover(decend.view:plantop().step)
		end
	end,

	menuopen = function(decend, menu)
		decend.view.menu = menu
		decend.view.menupos = 1
		menu.scroll = 0
		local init = decend.view.menu.init
		if init then
			init(decend)
		end
	end,

	menuhover = function(decend, idx)
		local view = decend.view
		local menu = decend.view.menu
		view.menupos = idx
		if menu.hover then
			menu.hover(decend, menu.item[idx], idx)
		end
	end,

	menuselect = function(decend, button)
		local mpos = decend.view.menupos
		assert(mpos)
		if button == 2 then
			decend.view.menu.func(decend, decend.view.menu.item[mpos], mpos)
		elseif decend.view.menu.aux then
			decend.view.menu.aux(decend, button, mpos)
		end
	end,
	
	menutitleobj = function(decend, str)
		local top = decend.view:plantop()
		decend.view.menu.title =
			decend:describe(top.creature, true)..": "..str
	end,
	
	-- returns true if pt is within the menu
	menumousemoved = function(decend, pt)
		local view = decend.view
		local pt = pt or { x = love.mouse.getX(), y = love.mouse.getY() }
		if view.menuwin and decend.util.inrect(view.menuwin.rect, pt) then
			for i,b in ipairs(view.menuwin.btn) do
				if decend.util.inrect(b, pt) then
					decend:menuhover(b.id)
					return true
				end
			end
			return true
		end
		return false
	end,

	actionrange = function(decend, a, o)
		local aname = a
		if type(a) == "table" then
			aname = a.t
		end
		if a == "cast" then return false end
		if aname == "cast" then
			return decend:spellrange(decend.data.spell[a.extra], o)
		end
		-- actions either have a fixed range, or have
		-- a range detirmined by a stat, or have
		-- unlimited range (nil)
		return decend:statref(decend.data.action[aname].r, o)
	end,

	statref = function(decend, ref, o)
		if type(ref) == "string" then
			ref = o.st[ref].v
		end
		return ref
	end,

	showrange = function(decend, c, r)
		local util = decend.util
		if c == nil then
			decend.view.range = nil
		else
			-- TODO: clamp within level bounds
			decend.view.range = util.rectclamp(
				util.floorbounds(decend.game.floor[decend.view.floor]),
				util.radius(c, r))
		end
	end,

	tilestr = function(decend, tile, long)
		if type(tile) == "string" then
			return decend.data.tile[tile][long and "s" or "c"]
		elseif tile.t == "stat" then
			if long then
				return "stat "..
					(tile.lvl < 0 and "drop" or "boost")..
					": "..tile.st.." "..
					decend.util.signfmt(tile.lvl)
			end
			local str = tile.st
			for i = 1,tile.lvl do
				str = str .. "+"
			end
			for i = tile.lvl,-1 do
				str = str .. "-"
			end
			return str
		end
		return "?TILE?"		
	end,
	
	spellcost = function(decend, spell)
		local sdata = decend.data.spell[spell.t]
		assert(sdata, "unknown spell: "..spell.t)
		-- TODO: reduce cost according to exp levels
		return sdata.cost
	end,
	
	spellrange = function(decend, spell, o)
		return decend:statref(decend.data.spell[spell.t].r, o)*
			decend.util.knows_spell(o, spell.t)
	end,
	
	putitem = function(decend, floorno, pos, itm)
		table.insert(decend.game.floor[floorno].item, itm)
		itm.pos = pos
	end,
	
	-- statistics, stats, and statuses are all different things.
	-- statistics track how much a certain thing was done in a given game.
	track = function(decend, name, amount)
		amount = amount or 1
		decend.game.statistic[name] = (decend.game.statistic[name] or 0) + amount
		local parent = string.match(name, "^(.+)%.[^%.]+")
		if parent then
			decend:track(parent, amount)
		end
	end,
	
	trackp = function(decend, name, amount)
		if decend.game.active == decend.game.player then
			decend:track(name, amount)
		end
	end,
	
	showstatistics = function(decend)
		local raw = love.filesystem.read("dat/stat.txt")
		decend:msg("= STATISTICS =")
		for id, label in string.gmatch(raw, "([^=]+)=([^\n]+)\n") do
			local v = decend.game.statistic[id]
			if v ~= nil and v ~= 0 then
				decend:msg(label..": "..v)
			end
		end
		-- for debugging purpouses
		print("== RAW STATISTICS ==")
		local slist = decend.util.keys(decend.game.statistic)
		table.sort(slist)
		for _, id in ipairs(slist) do
			print(id.."="..decend.game.statistic[id])
		end
	end,
	
	objcolor = function(decend, o)
		if type(o) ~= "table" then
			return { 1, 1, 1 }
		elseif o == decend.game.player then
			return { 0, 0, 1 }
		elseif o.spell == nil then -- no spell list, this is an item
			return { 1, 1, 0 }
		elseif o.st.hp.v < 0 then
			return { 106/255, 2/255, 119/255 }
		elseif o.st.aff.v > 0 then
			return { 0, 0.5, 0 }
		else
			return { 0.8, 0, 0 }
		end
	end,
	
	anim_capture = function(decend, floor)
		local anim, game = decend.anim, decend.game
		local tbl = { pos = {}, hp = {} }
		assert(floor.obj)
		for _,o in ipairs(floor.obj) do
			tbl.pos[o.id] = o.pos
			tbl.hp[o.id] = o.st.hp.v
		end
		anim.turn[game.turn] = tbl
	end,
}
