-- round number, action type, targ.x, targ.y, obj, extra
local demo_fmt = "<JsJJJs"

-- replay/demo system
return {
	data = {},
	mode = nil,

	record = function(decend)
		decend.demo.mode = "record"
		decend.demo.data = {
			seed = decend.game.seed,
			round = {},
			timestamp = os.date("%FT%H%M%S"),
		}
	end,
	
	replay = function(decend)
		decend.demo.mode = "replay"
	end,
	
	capture = function(demo, plan, game)
		assert(demo.data.round[game.round] == nil)
		demo.data.round[game.round] = {
			plan = demo:cleanplan(plan),
			state = game.rng:getState(),
		}
	end,
	
	playback = function(demo, game)
		local rdata = demo.data.round[game.round]
		assert(rdata)
		assert(rdata.plan)
		if rdata.state ~= game.rng:getState() then
			print("WARN: rng desync!")
			game.rng:setState(rdata.state)
		end
		return demo:tangleplan(rdata.plan, game.obj)
	end,
	
	cleanplan = function(demo, plan)
		local p = {}
		for i, step in ipairs(plan) do
			p[i] = {}
			p[i].t = step.t
			if step.targ then
				p[i].targ = { x = step.targ.x, y = step.targ.y }
			end
			if step.obj then
				p[i].obj = step.obj.id
			end
			if step.t == "order" then
				p[i].extra = demo:cleanplan(step.extra)
			else
				p[i].extra = step.extra
			end
		end
		return p
	end,
	
	tangleplan = function(demo, plan, olist)
		local p = {}
		for i, step in ipairs(plan) do
			p[i] = {}
			p[i].t = step.t
			p[i].targ = step.targ
			if step.obj then
				p[i].obj = olist[step.obj]
			end
			if step.t == "order" then
				p[i].extra = demo:tangleplan(step.extra, olist)
			else
				p[i].extra = step.extra
			end
		end
		return p
	end,
	
	save = function(demo)
		local cyclini = require("lib.cyclini")
		love.filesystem.createDirectory("demo")
		local f, err = love.filesystem.newFile("demo/decend_"..demo.data.timestamp..".ini", "w")
		if f == nil then error(err) end
		local ok, err = f:write(cyclini.marshal(demo.data))
		if not ok then error(err) end
		f:close()
	end,
	
	-- TODO: load() replays, also a menu that allows you to select a replay to load
}

