return {
	data = {
		-- all messages
		full = {},
		-- floating messages
		active = {},
		-- offset in .full where messages from the current round start
		round_offset = 0,
		
	},
	
	beginround = function(logs)
		logs.data.active = {}
		logs.data.round_offset = #logs.data.full
	end,
	
	-- TODO: message priority system, you would then be able to adjust it so only messages past a certain priority would show up above creature's heads
	-- TODO: once complete message log is also onscreen, add ability to click floating message to see it in context.
	msg = function(logs, o, text)
		-- we hold onto the object reference so we can deref to pos later,
		-- this way it tracks if the object moves
		local itm = {obj = o, text = text}
		if o then
			table.insert(logs.data.active, itm)
		end
		table.insert(logs.data.full, itm)
	end,
	
	layout = function(logs)
		local r = {}
		local cache = {}
		for _,itm in ipairs(logs.data.active) do
			local id = itm.obj.pos.x + itm.obj.pos.y*10000
			local list = cache[id] or {}
			table.insert(list, itm)
			if not cache[id] then
				table.insert(r, { pos = itm.obj.pos, list = list })
				cache[id] = list
			end
		end
		return r
	end,
}
