-- TODO: creature with a unique immunity to crits
-- TODO: spell that can decrease/increase a creatrue's affinity greatly (expensive), to counter the player just relying on one really strong creature
-- TODO: spell that moves a creature to your location
-- TODO: spell that allows you to defend a creature from any range
-- TODO: spells that are unlimited-range versions of all the basic actions
-- TODO: "levitating/floating" status condition inflicted by a spell, prevents you from interacting with things on the ground (items(?), tiles, staircases...)
return {
	-- list of stats
	st = {
		-- health, max hp also doubles as a weight stat?
		hp = {
			-- default value
			d = 10,
			-- how much the stat increases over time as you stay on a floor
			scale = 2,
			-- this is a "bar" stat, it has a maximum and current value
			isbar = true,
		},
		mana = {
			d = 0,
			scale = 2,
			isbar = true,
		},
		atk = {
			d = 1,
			scale = 1.5,
		},
		def = {
			d = 0,
			scale = 1,
			learn_exp = 2,
		},
		-- reach/attack range
		rch = {
			d = 1,
			scale = 0.4,
			learn_exp = 3,
		},
		will = {
			d = 5,
			scale = 3,
		},
		-- inteligence, affects several things, such as:
		-- * chance to learn spells
		-- * chance to crit when attacking
		-- * chance to be inflicted with a status condition
		int = {
			d = 10,
			scale = 2,
		},
		-- TODO: remove this
		cha = {
			d = 5,
		},
		-- action points, number of actions in a turn.
		-- i guess lore-wise this is like, short term endurance, how much you can do before you need a short break.
		act = {
			d = 1,
			scale = 0.3,
			-- exponent used in the learnfrom formula
			-- increasing it makes it harder to get a random increase to the stat
			learn_exp = 4,
		},
		-- movement speed, how fast a creature can run.
		mov = {
			scale = 3,
			d = 5,
		},
		-- speed, detirmines turn order.
		-- lore reasons for it being seperate: this represents not just physical speed, but also initiative and quick thinking.
		spd = {
			d = 10,
			scale = 2,
		},
		-- affinity (tempted to rename to "hope")
		aff = {
			d = 0,
		},
	},
	
	-- order in which to show stats
	st_list = {
		"hp", "mana",
		"atk", "def", "rch",
		"will", "int",
		"spd", "mov", "act",
		"aff",
	},

	-- stats that can be learned, order affects rng
	st_learn_list = {
		"hp", "mana",
		"atk", "def", "rch",
		"will", "int",
		"spd", "mov", "act",
	},
		
	-- stats that can be changed by stat-changing tiles
	st_tile_list = {
		"hp", "mana",
		"atk", "def",
		"will", "int",
		"mov", "spd",
	},

	-- TODO: some sort of preset system for things like "undead" that adds a set
	-- of standard weaknesses/resistances.  maybe call it a "group"?  "flying" group, "bug" group, etc..
	creature = require("src.data.creature"),
	
	-- BUG: action priority is not working properly.
	action = {
		move = {
			q = "move where?",
			r = "mov",
		},
		attack = {
			q = "attack what?",
			r = "rch",
		},
		aid = {
			q = "aid whom?",
			r = 1,
		},
		throw = {
			q = "throw where?",
			r = "atk",
		},
		grab = {
			q = "grab what?",
			r = 1,
		},
		order = {
			q = "order whom?",
			-- action has infinite range.
			r = nil,
			-- "order" has extra priority.
			pri = 10,
		},
		cast = {
			q = "cast on what?",
		},
		-- arguably shouldnt lock onto targets, but "move here/climb self" is
		-- a fairly useful pattern sometimes...
		climb = {
			q = "climb what?",
			r = 0,
			pri = -1,
		},
		-- WARN: auto cannot choose an action with higher priority
		auto = {
			-- action has no range, it always self-targets
			r = false,
		},
	},

	-- TODO: fields that specify how much the spell level increases certain fields.  maybe even just like a "exp_mul" subtable that has keys saying how much to multiply by?
	spell = {
		embargo = {
			cost = 2,
			status = { t = "no_aid", d = 1 },
			r = "rch",
		},
		pacify = {
			cost = 2,
			status = { t = "no_attack", d = 1 },
			r = "rch",
		},
		restrain = {
			cost = 2,
			status = {{ t = "no_move", d = 1 }, { t = "no_climb", d = 1 }},
			r = "rch",
		},
		silence = {
			cost = 2,
			status = {{ t = "no_order", d = 1 }, { t = "no_cast", d = 1 }},
			r = "rch",
		},
		cower = {
			cost = 2,
			status = {{ t = "pity", d = 3 }},
			r = 7,
		},
		absorb = {
			cost = 2,
			dmg = "atk",
			attack = "acid",
			r = "rch",
			lifesteal = 0.25,
		},
		decimate = {
			cost = 2,
			dmg = true,
			attack = "fixed",
			r = "rch",
		},
		examine = {
			cost = 1,
			status = {{ t = "vuln", d = 2 }},
			r = 100,
		},
		protect = {
			cost = 1,
			r = 100,
			-- the spell benefits the targeted creature
			helpful = true,
		},
		ensnare = {
			cost = 2,
			status = {{ t = "slow", d = 3}},
			r = "spd",
		},
		drain = { -- vampire spell
			cost = 6,
			dmg = "atk",
			attack = "bite",
			r = 1,
			lifesteal = 0.5,
		},
		-- a strong pressure wave, similar to one created by an explosion
		blastwave = {
			cost = 3,
			dmg = "atk",
			r = "rch",
			attack = "fling",
		},
		-- TODO: "bisect" spell that removes 1/2 the targets current hp.
		-- TODO: spell that affects affinity
		-- TODO: "warp" spell that allows you to move instantly (give to wisp)
		-- TODO: spell that allows you to push a creature, maybe fling them back and deal fall damage?  actually, maybe there should just be a damage type that knocks you back?
		-- TODO: spell that sets a creatures "aggro" field to nil
		-- TODO: "lullaby" spell that puts a creature to sleep (the sleep condition is removed if the creature takes damage)
		-- TODO: aoe spells
		-- TODO: endgame "kill" spell, has a cost equal to the target's hp.  deals an amount of "fixed" damage equal to the target's hp stat.  also has a short range.
		-- TODO: multihitting spell
	},
	
	-- damage types
	effect = {
		-- fire deals half health over 4 turns
		fire = {
			status = { t = "burn", d = 3 },
		},
		-- poison deals full health over 16 turns
		poison = {
			status = { t = "poison", d = 15 },
		},
		bite = {
			crit = 2,
		},
		fixed = {
			crit = 0,
			-- multiplyer to the effectivness of the defense stat
			def = 0,
		},
		fling = {},
		sound = { def = 0.5 },
		curse = { def = 0, crit = 0 },
		magic = {},
		acid = {},
	},

	status = {
		pity = {
			st = { atk = 0.5, will = 0.5 },
		},
		burn = {
			st = {},
			-- what fraction of a creatures health do deal at end of round
			dot = 8,
		},
		poison = {
			st = {},
			dot = 16,
		},
		slow = {
			st = { mov = 0.5, spd = 0.5 },
		},
	},

	-- TODO: golf club that deals fling damage?
	item = {
		knife = {
			attack = "pierce",
			st = { atk = 2 },
		},
		torch = {
			attack = "fire",
			st = { atk = 2 },
		},
		whip = {
			attack = "slash",
			st = { atk = -1, rch = 3 },
		},
		spear = {
			attack = "pierce",
			st = { atk = 1, rch = 1 },
		},
		buckler = {
			st = { def = 1 },
		},
		book = {
			st = { int = 10 },
		},
		club = {
			attack = "impact",
			st = { atk = 4, spd = -2 },
		},
		axe = {
			attack = "slash",
			st = { atk = 6, spd = -5 },
		},
		salve = { -- restores any creature to full hp once
			st = {},
		},
		-- TODO: mana equivelant of salve
		-- TODO: "holy water" item, removes all statuses and damages undead
		crossbow = {
			attack = "pierce",
			st = { atk = 2, rch = 15, spd = -7 },
			charge = 10,
		},
		sling = {
			attack = "impact",
			st = { atk = 1, rch = 10, spd = -3 },
			charge = 20,
		},
		longbow = {
			attack = "pierce",
			st = { atk = 3, rch = 20, spd = -4 },
			charge = 10,
		},
		revolver = {
			attack = "pierce",
			st = { atk = 20, rch = 40, spd = 5 },
			charge = 6,
		},
		-- repository of magical energy, allows you to store more mana
		amulet = {
			st = { mana = 20 },
		},
		wristwatch = {
			st = { act = 1 },
		},
		memento = {
			st = { will = 20 },
		},
		nine_iron = {
			st = { atk = 2 },
			attack = "fling",
		},
	},

	-- TODO: could make certain creatures/items more common by listing them multiple times.
	area = {
		sewer = {
			spawn = {
				"slime",
				"rat",
				"scorpion",
				"turtle",
			},
			item = {
				"knife",
				"torch",
				"whip",
				"buckler",
				"spear",
				"club",
				"salve",
				"sling",
				"nine_iron",
			},
		},
		crypt = {
			spawn = {
				"skeleton",
				"zombie",
				"vampire",
				"wisp",
			},
			item = {
				"book",
				"club",
				"knife",
				"spear",
				"salve",
				"crossbow",
				"wristwatch",
			},
		},
		cave = {
			spawn = {
				"spider",
				"bat",
				"slug",
			},
			item = {
				"revolver",
				"torch",
				"crossbow",
				"amulet",
				"memento",
			},
		},
		-- TODO: "labrynth" area
		-- TODO: "aquafer" area
	},

	floor = {
		[1] = { area = "sewer" },
		[4] = { area = "crypt" },
		[7] = { area = "cave" },
	},
	
	-- creature attributes, these modify stats
	attr = {
		-- HP boosting
		large = {
			-- being bigger means bigger footsteps, so movement stays the same,
			-- but spd is decreased, as you cannot react and make quick movements as much
			hp = 1.5,
			spd = 0.5,
		},
		giant = {
			hp = 2,
			spd = 0.25,
		},
		young = {
			int = 0.5,
			hp = 2,
		},

		-- MANA boosting
		glowing = {
			mana = 4,
			spd = 0.5,
		},
		wizened = {
			hp = 0.75,
			mana = 2,
		},

		-- ATK boosting
		reckless = {
			atk = 2,
			def = 0.5,
		},
		angry = {
			atk = 2,
			int = 0.5,
		},
		feral = {
			atk = 2,
			will = 0.5,
		},		

		-- DEF boosting
		fearful = {
			atk = 0.8,
			def = 2,
			will = 0.5,
		},
		careful = {
			atk = 0.5,
			def = 2,
		},
		bulky = {
			def = 2,
			spd = 0.5,
		},

		-- RCH boosting
		spindly = {
			rch = 2,
			hp = 0.75,
		},
		
		-- WILL boosting
		steadfast = {
			will = 2,
			mov = 0.5,
		},
		resolute = {
			will = 2,
			spd = 0.5,
		},
		headstrong = {
			will = 2,
			def = 0.5,
		},
		naive = {
			will = 2,
			int = 0.8,
		},
		stubborn = {
			will = 3,
			int = 0.4,
		},
		emptyheaded = {
			will = 5,
			int = 0.1,
		},
		

		-- INT boosting
		disillusioned = {
			will = 0.5,
			int = 1.5,
		},
		old = {
			int = 2,
			hp = 0.5,
		},
		shy = {
			int = 2,
			spd = 0.25, -- shy, so they go last
		},
		inquisitive = {
			int = 2,
			def = 0.5,
		},


		-- ACT
		overworked = {
			act = 1.5,
			hp = 0.5,
			spd = 0.5,
		},
		desperate = {
			act = 1.5,
			-- desperation makes you lower you guard and act irrationaly
			def = 0.5,
			int = 0.5,
		},

		-- MOV
		nimble = {
			atk = 0.75,
			mov = 2,
		},
		lightweight = {
			mov = 2,
			dev = 0.5,
		},
		-- TODO: something that would describe a olimpic sprinter, able to run fast, but low endurance.  mov+++, act-

		-- SPD
		spry = {
			spd = 2,
			
		},
		small = {
			hp = 0.75,
			spd = 2,
		},
		decisive = {
			spd = 2,
			int = 0.8,
		},
		
		-- MISC
		feckless = {
			will = 0.5,
		},
		doomed = {
			will = 0,
		},
		relentless = {
			atk = 2,
			will = 2,
			int = 2,
			spd = 2,
			mov = 2,
			act = 0.5,

		},
		elite = {
			hp = 1.2,
			mana = 1.2,
			atk = 1.2,
			def = 1.2,
			int = 1.2,
			will = 1.2,
			mov = 1.2,
			spd = 1.2,
		},
	},

	tile = {
		ustair = {
			c = "^",
			s = "upwards staircase",
		},
		dstair = {
			c = "v",
			s = "downwards staircase",
		},
	},

	-- all attributes in alphabetical order,
	-- used so they can be chosen at random.
	attr_list = nil,
}

