return {
	list = {},
	known = {}, -- tips that have already been told to the player
	lastkeyword = false, -- keyword of the last tip

	init = function(tips)
		local raw = love.filesystem.read("dat/tips.txt")
		local new = nil
		for l in string.gmatch(raw, "([^\n]*)\n") do
			if string.len(l) ~= 0 and string.sub(l, 1, 1) ~= "#" then
				local id, op, n = string.match(l, "^%s+(%g+)([=<>])(%d+)")
				if id then
					table.insert(new.cond, { id = id, op = op, n = tonumber(n) })
				else
					table.insert(tips.list, new)
					new = { text = l, cond = {} }
				end
			end
		end
		local ok, err = tips:load()
		if not ok then
			print("WARNING: failed to load tip history: "..tostring(err))
		end
	end,
	
	-- given a table of statistics, choose an appropriate tip
	choose = function(tips, tbl, tipconf)
		for idx,tip in ipairs(tips.list) do
			local ok = true
			local n = 1
			for _,c in ipairs(tip.cond) do
				local v = tbl[c.id] or 0
				if
					(c.op == "=" and v == c.n) or
					(c.op == "<" and v < (c.n or 0)) or
					(c.op == ">" and v > (c.n or 0))
				then
				else
					ok = false
				end
			end
			if ok then n = -tipconf.dec_timeout end
			local keyword = string.match(tip.text, "%[(%w)%]")
			ok = ok and keyword ~= tips.lastkeyword and not tips.known[idx]
			-- only affect tip timeout if a tip is actually on timeout
			if tips.known[idx] then
				tips.known[idx] = math.min(
					tipconf.max_timeout,
					tips.known[idx] + n)
				if tips.known[idx] <= 0 then tips.known[idx] = nil end
			end
			if ok then
				tips.lastkeyword = keyword
				tips.known[idx] = tipconf.ret_timeout 
				return tip
			end
		end
		-- failsafe, no tips were found
		return { text = "try asking the dev to add more tips." }
	end,
	
	save = function(tips)
		local cyclini = require("lib.cyclini")
		local f, err = love.filesystem.newFile("tiphist.ini", "w")
		if f == nil then error(err) end
		-- cyclini.marshal() actually returns 2 values, so
		-- we cannot pass it directly to write()
		local raw = cyclini.marshal({
			known = tips.known,
			count = #tips.list,
			lastkeyword = tips.lastkeyword,
		})
		local ok, err = f:write(raw)
		if not ok then error(err) end
		f:close()
	end,
	
	load = function(tips)
		local cyclini = require("lib.cyclini")
		local raw, err = love.filesystem.read("tiphist.ini")
		if raw == nil then
			return false, err
		end
		local dat, err = cyclini.unmarshal(raw)
		if dat == nil then
			return false, err
		end
		-- if more tips have been added, the index of the tips may have been shuffled around, so don't load tip history.
		if dat.count ~= #tips.list then
			return false, "number of tips changed"
		end
		tips.known = dat.known
		tips.lastkeyword = dat.lastkeyword
		return true, nil
	end,
}
