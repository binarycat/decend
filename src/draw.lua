local text_cache = setmetatable({}, {__mode = "kv"})

return {
	print = function(str, x, y, right_align, bottom_align)
		-- this assumes the font will never change!
		local text = text_cache[str]
		if text == nil then
			text = love.graphics.newText(love.graphics.getFont(), str)
			text_cache[str] = text
		end
		local tw = text:getWidth()
		local th = text:getHeight()
		local bdr = th * 0.2
		local bw = tw+2*bdr
		local bh = th+2*bdr
		local lh = th*1.6
		if right_align then x = x - bw end
		if bottom_align then y = y - lh end
		x = math.floor(x)
		y = math.floor(y)
		local bx = x-bdr
		local by = y-bdr
		love.graphics.setColor(0, 0, 0, 0.6)
		-- TODO: rounded corners
		love.graphics.rectangle("fill", bx, by, bw, bh)
		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.draw(text, x, y)
		return lh, {
			min = { x = bx, y = by },
			max = { x = bx+bw, y = by+bh },
		}
	end,

	declutter_mode = function(draw)
		return love.keyboard.isDown("lshift")
	end,
	
	-- draw a scroll bar.
	scroll_bar = function(x, y, w, h, off, sz, rect_out)
		off = off*(1-sz)
		love.graphics.setColor(0.2, 0.2, 0.2)
		love.graphics.rectangle("fill", x, y, w, h)
		love.graphics.setColor(0.7, 0.7, 0.7)
		love.graphics.rectangle("line", x, y, w, h)
		love.graphics.rectangle("fill", x, y+h*off, w, h*sz)
		if rect_out then
			rect_out.min.x = x
			rect_out.min.y = y
			rect_out.max.x = x+w
			rect_out.max.y = y+h
		end
		return off
	end,

	selinfo = function(decend)
		local selobj = decend.view.selobj
		local sel = decend.view.sel
		local x, y = 10, 10
		local p = function(str)
			y = y + decend.draw.print(str, x, y)
		end
		if selobj then
			p(decend:describe(selobj), x, y)
			for _,st in ipairs(decend.data.st_list) do
				local str = string.upper(st)..": "..selobj.st[st].v
				local stat = selobj.st[st]
				if selobj.st[st].max then
					str = str.." / "..selobj.st[st].max
				elseif st ~= "aff" and stat.v ~= stat.base then
					str = str.." ("..decend.util.signfmt(stat.v-stat.base)..")"
				end
				p(str)
			end
			p("")
			if selobj.item then
				p("held: "..decend:describe(selobj.item, true))
			end
			if selobj.guard then
				p("guard: "..decend:describe(selobj.guard))
			end
			for _,s in ipairs(selobj.status) do
				p("status: "..s.t.." ("..s.d..")")
			end
			if selobj.order then
				p("orders given "..(decend.game.round-selobj.order.round).." round(s) ago")
				-- TODO: show plan of creature if you ordered it?
			end
			p("")
			p("")
		end
		local floorno = decend.view.floor
		local seltile = decend:tileunder({floor = floorno, pos = sel})
		if seltile then
			p("tile: "..decend:tilestr(seltile, true))
		end
		local selitems = decend:ontile(floorno, sel, true, "item")
		for _,itm in ipairs(selitems) do
			p("item: "..itm.t)
			local idata = decend.data.item[itm.t]
			if idata.attack then
				p("  "..idata.attack.." weapon")
			end
			if itm.charge then
				p("  "..itm.charge.." ammo")
			end
			for _,st in ipairs(decend.data.st_list) do
				local n = idata.st[st]
				if n and n ~= 0 then
					p("  "..st.." "..decend.util.signfmt(idata.st[st]))
				end
			end
		end
	end,
	
	float_msgs = function(decend)
		local gridsize = decend.view.gridsize
		for _,tbl in ipairs(decend.logs:layout()) do
			local pos = decend:tile2pixel(tbl.pos)
			local x = pos.x + gridsize/4
			local y = pos.y - gridsize/2
			if decend.draw:declutter_mode() then
				local sel = decend.view.sel
				if sel and sel.x == tbl.pos.x and sel.y == tbl.pos.y then
					-- fall through and draw item
				else
					goto next
				end
			end
			for _,itm in ipairs(tbl.list) do
				if itm.obj.floor == decend.view.floor then
					y = y - decend.draw.print(itm.text, x, y)
				end
			end
			::next::
		end
	end,
	
	-- TODO: allow collapsing groups of messages like a <details> tag
	-- TODO: word wrap?
	-- TODO: optimize this so we don't draw every message each frame.
	msg_log = function(decend)
		local view = decend.view
		local barw = decend.conf.scroll_bar_width
		if decend.draw:declutter_mode() then return end
		if decend.view.log_rect == nil then return end
		local log_rect = decend.view.log_rect
		local x = log_rect.max.x
		local y = log_rect.max.y
		local log_list = decend.logs.data.full
		local i = #log_list
		local stop = decend.logs.data.round_offset
		if decend.view.log_scroll then
			stop = 0
			y = y + decend.view.log_scroll
		end
		local lineh = 0
		local fullh = 0
		while i > stop do
			lineh = decend.draw.print(log_list[i].text, x-barw, y, true, true)
			fullh = fullh + lineh
			y = y - lineh
			i = i - 1
		end
		local logh = log_rect.max.y-log_rect.min.y
		-- size of the scroll handle within the bar
		local bar_size = math.min(1, logh/fullh)
		local bar_off = 1 - (view.log_scroll or 0)/fullh
		bar_off = decend.draw.scroll_bar(
			x-barw, 0, barw, logh,
			bar_off,
			bar_size,
			decend.view.log_scroll_bar_rect)
		local targ = view.log_scroll_targ
		if targ == nil then
			-- do nothing
		elseif bar_off < targ and targ < bar_off + bar_size then
			-- do nothing
		elseif bar_off + bar_size > targ then
			-- scroll up
			view.log_scroll =
				(view.log_scroll or 0) +
				decend.conf.scroll_bar_speed * 
				(bar_size + bar_off - targ)/bar_size
		elseif bar_off < targ then
			-- scroll down
			view.log_scroll =
				(view.log_scroll or 0) -
				decend.conf.scroll_bar_speed *
				(targ - bar_off)/bar_size
		end
	end,
		
	menu = function(decend)
		if not decend.view.menu then return end
		local scrw, scrh = love.graphics.getDimensions()
		local menuw = scrw/3
		local menuh = scrh*2/5
		local menux = scrw - menuw
		local menuy = scrh - menuh
		local lineh = 30
		local mwin = {
			rect = {
				min = { x = menux, y = menuy },
				max = { x = scrw, y = scrh },
			},
			btn = {},
		}
		local col = decend.view.menu.column or 1
		-- TODO: draw border?
		love.graphics.setColor(0.05, 0.05, 0.05)
		love.graphics.rectangle("fill", menux, menuy, menuw, menuh)
		love.graphics.setColor(0.9, 0.9, 0.9)
		love.graphics.print(decend.view.menu.title,
							menux + 20, menuy + 5)
		
		for k, v in ipairs(decend.view.menu.label or decend.view.menu.item) do
			local xi = (k+1) % col
			local yi = math.floor((k+1) / col) + math.floor(decend.view.menu.scroll)
			if col == 1 then yi = yi - 1 end
			if yi > 0 then
				local liney = menuy + lineh*yi + 30
				local linex = menux + 15 + (menuw/col*xi)
				local _, nbtn = decend.draw.print(v, linex, liney)
				if k == decend.view.menupos then
					love.graphics.print("•", linex - 10, liney)
				end
				nbtn.id = k
				table.insert(mwin.btn, nbtn)
			end
		end
		decend.view.menuwin = mwin
		decend.view.log_rect = {
			min = { x = menux, y = 0 },
			max = { x = scrw, y = menuy },
		}
	end,
}
