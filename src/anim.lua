return {
	-- TODO: animate log messages being printed
	-- TODO: animate hp (and other stats?) damage numbers?
	-- TODO: animate actions of all creatures in a round individually?
	-- record of the state of things at the start of each turn in this round
	turn = {},
	-- animation stack, tells the current animation state
	stack = {},
	
	duration = {
		move = 20,
	},
	
	play = function(anim, name, opt)
		table.insert(anim.stack, { t = name, prog = 0, opt = opt })
	end,
	
	-- position of object for current animation state
	objpos = function(anim, o)
		local util = require("src.util")
		local prog = 1
		local top = anim.stack[#anim.stack]
		-- if there is no movement animation, return real pos
		if anim.turn[1] == nil or top == nil or top.t ~= "move" then
			return o.pos
		end
		prog = top.prog
		local dst = o.pos
		local nextturn = anim.turn[top.opt.turn+1]
		if nextturn then dst = nextturn.pos[o.id] or dst end
		return util.interp_pos(anim.turn[top.opt.turn].pos[o.id], dst, prog)
	end,
	
	update = function(anim)
		local top = anim.stack[#anim.stack]
		if top == nil then return end
		top.prog = top.prog + 1/anim.duration[top.t]
		if top.prog > 1 then
			anim.stack[#anim.stack] = nil
		end
	end,
	
	-- play animations for the round
	play_round = function(anim)
		for i = #anim.turn,1,-1 do
			anim:play("move", {turn = i})
		end
	end,
}
