-- TODO: when choosing action and target, add info of who is doing what action to the title.
-- TODO: keyboard shortcuts for choosing actions?  maybe use number keys as a generic "choose nth menu option" shortcut?

return {
	action = {
		column = 2,
		title = "choose action",
		item = {
			"move",
			"climb",
			"attack",
			"cast",
			"aid",
			"grab",
			"throw",
			"order",
			"auto",
			"cancel",
		},
		init = function(decend)
			decend:menutitleobj("choose action")
		end,
		func = function(decend, itm)
			local top = decend.view:plantop()
			if itm == "cancel" then
				decend:menuplan()
				return
			end
			if type(itm) == "string" then
				top.plan[top.step] = {
					t = itm,
					targ = nil,
				}
				if itm == "cast" then
					decend:menuopen(decend.menu.cast)
					-- TODO: show spell range
				elseif decend.data.action[itm].r == false then
					top.plan[top.step].targ = top.creature.pos
					decend:menuplan()
				else
					decend:menuopen(decend.menu.target)
					local r = decend:actionrange(itm, top.creature)
					if r then
						decend:showrange(top.creature.pos, r)
					end
				end
			end
		end,
	},

	cast = {
		title = "cast what?",
		init = function(decend)
			local top = decend.view:plantop()
			local itemlist = {}
			local labellist = {}
			for _,sp in pairs(top.creature.spell) do
				table.insert(itemlist, sp.t)
				table.insert(labellist,
					"["..decend:spellcost(sp).."] "..sp.t..
					" ("..decend:spellrange(sp, top.creature)..")")
			end
			table.sort(itemlist)
			table.insert(itemlist, "cancel")
			table.insert(labellist, "cancel")
			decend.view.menu.item = itemlist
			decend.view.menu.label = labellist
			decend:menutitleobj("cast what?")
		end,
		func = function(decend, itm)
			if type(itm) == "string" then
				if itm == "cancel" then
					decend.view:plancancel()
					decend:menuplan()
				else
					local step = decend.view:planstep()
					step.extra = itm
					decend:menuopen(decend.menu.target)
				end
			end
		end,
		-- TODO: show spell range when hovering, similar to how the action ranges work.
	},

	target = {
		-- TODO: option to cancel
		title = "choose target",
		item = {},
		init = function(decend)
			decend:menutitleobj(decend.data.action[decend.view:planstep().t].q)
		end,
		func = function(decend, itm)
			if type(itm) == "table" then
				local top = decend.view:plantop()
				local step = top.plan[top.step]
				step.targ = itm
				-- TODO: now that we are locking on to creatures here, we prolly don't need to be doing it in think() and perform()
				step.obj = decend:ontile(top.creature.floor, step.targ)
				decend.view.range = nil
				if step.t == "order" then
					local c = step.obj
					if not c then
						decend:msg("nothing to order")
					else
						decend:menuplan(c)
						return
					end
				end
				decend:menuplan()
			end
		end
	},
	
	plan = {
		title = "editing plan",
		item = {},
		init = function(decend)
			local menu = decend.view.menu
			local top = decend.view:plantop()
			menu.item = {}
			for i = 1,top.creature.st.act.v do
				table.insert(menu.item, "turn "..tostring(i)..": "..
							 decend:describe_step(top.plan[i], top.creature))
			end
			if #decend.view.planstack == 1 then
				table.insert(menu.item, "proceed")
			else
				table.insert(menu.item, "done")
			end
			menu.title = "editing plan of "..decend:describe(top.creature)
		end,
		func = function(decend, itm, idx)
			if type(itm) == "table" then return end
			if itm == "done" or itm == "proceed" then
				if #decend.view.planstack == 1 then
					local p = decend.view.planstack[1]
					p.creature.plan = p.plan
					decend.view.planstack = {}
					decend.demo:capture(p.plan, decend.game)
					decend:update()
					decend.anim:play_round()
					decend.view.floor = decend.game.player.floor
					decend:menumousemoved()
				else
					local order = decend.view:plantop()
					decend.view.planstack[#decend.view.planstack] = nil
					local top = decend.view:plantop()
					top.plan[top.step].extra = order.plan
					top.plan[top.step].obj = order.creature
					decend:menuplan()
				end
			else
				decend.view:plantop().step = idx
				decend:menuopen(decend.menu.action)
			end
		end,
		hover = function(decend, itm, idx)
			local step = decend.view:plantop().plan[idx]
			-- this assumes a creature will never have more steps in
			-- its plan than it has action points
			if step then
				decend:showrange(step.targ, 0)
			else
				decend.view.range = nil
			end
		end,
		-- handle other mouse buttons
		aux = function(decend, button, idx)
			local plan = decend.view:plantop().plan
			if button == 3 then
				plan[idx] = plan[idx - 1]
			elseif button == 4 then
				plan[idx] = plan[idx + 1]
			end
			decend.view.menu.init(decend)
		end,
	},
	
	-- TODO: the game over menu should appear on a different part of the screen, making it harder to accidentally spam click past.
	dead = {
		title = "game over!",
		item = {
			"new",
			"retry",
			"watch replay",
			"save replay",
		},
		func = function(decend, itm)
			if itm == "save replay" then
				decend:msg("saving replay...")
				-- TODO: wrap this in a pcall
				decend.demo:save()
				decend:msg("replay saved!")
			elseif itm == "watch replay" then
				decend.demo.replay(decend)
				decend:embark(decend.demo.data.seed)
				decend:menuopen(decend.menu.replay)
			else
				decend.demo.mode = nil
				if itm == "new" then
					decend:embark()
				elseif itm == "retry" then
					decend:embark(decend.game.seed)
				end
				decend:menuplan(decend.game.player)
			end
			decend.game.active = decend.game.player
			decend.view.floor = 1
		end,
	},
	
	replay = {
		title = "watching replay",
		item = {
			"next round",
		},
		func = function(decend, itm)
			if itm == "next round" then
				decend.game.player.plan = decend.demo:playback(decend.game)
				decend:update()
			end
		end,
	}
}
